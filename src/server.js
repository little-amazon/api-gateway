'use strict';

const express = require('express');
const router = require('./router');
const bodyParser = require('body-parser'); //to correct parse json from body

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

//add json parser POST function
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//add routers
app.use(router);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
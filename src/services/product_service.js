var express = require('express');
var router = express.Router()

//create adapter to forward calls to the right service
const apiAdapter = require('./api_adapter');
const BASE_URL = 'http://' + process.env.GATEWAY_PRODUCT_URL + ':8080';
const api = apiAdapter(BASE_URL);



router.get('/product', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/product', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.post('/product', (req, res) => {
    api.post(req.path, req.body).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.get('/product/:productID', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/product/:productID', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.put('/product/:productID/:quantity', (req, res) => {
    api.put(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/product/:productID/:quantity', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

module.exports = router

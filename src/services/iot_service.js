var express = require('express');
var router = express.Router()

//create adapter to forward calls to the right service
const apiAdapter = require('./api_adapter');
const BASE_URL = 'http://' + process.env.GATEWAY_IOT_URL + ':8080';
const api = apiAdapter(BASE_URL);



router.get('/iot/association', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/iot/association', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.post('/iot/association', (req, res) => {
    api.post(req.path, req.body).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.get('/iot/user/:userID', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/iot/association/:associationID', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});


module.exports = router

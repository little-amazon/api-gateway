var express = require('express');
var router = express.Router()

//create adapter to forward calls to the right service
const apiAdapter = require('./api_adapter');
const BASE_URL = 'http://' + process.env.GATEWAY_ORDER_URL + ':8080';
const api = apiAdapter(BASE_URL);



router.get('/order', (req, res) => {
    console.log(BASE_URL, req.path);
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});


router.delete('/order', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.post('/order', (req, res) => {
    api.post(req.path,req.body).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.get('/order/:orderID', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/order/:orderID', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.get('/order/user/:userID', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.get('/order/:orderID/amount/:currency', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});


module.exports = router

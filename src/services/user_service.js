var express = require('express');
var router = express.Router()

//create adapter to forward calls to the right service
const apiAdapter = require('./api_adapter');
const BASE_URL = 'http://' + process.env.GATEWAY_USER_URL + ':8080';
const api = apiAdapter(BASE_URL);



router.get('/user', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.get('/user/:userID', (req, res) => {
    api.get(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/user/:userID', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.delete('/user', (req, res) => {
    api.delete(req.path).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.post('/user', (req, res) => {
    api.post(req.path,req.body).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

router.post('/user/login', (req, res) => {
    api.post(req.path,req.body).then(resp => {
        res.send(resp.data)
    }).catch(function (error) {
        
        console.log(error);
        // handle error
        res.status(400).send(error.response.data);
      });
});

module.exports = router

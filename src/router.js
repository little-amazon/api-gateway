const Router = require('express').Router;
const productService = require('./services/product_service');
const orderService = require('./services/order_service');
const iotService = require('./services/iot_service');
const userService = require('./services/user_service.js');

//init router
const router = Router();

//here auth requests
router.use((req, res, next) => {
    console.log("HEI QUI Called: ", req.path)
    next()
});

router.use(userService);
router.use(productService);
router.use(orderService);
router.use(iotService);


module.exports = router;

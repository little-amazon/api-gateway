# api-gateway
This is the api-gateway written in NodeJS for the little-amazon service.

# Description
This api-gateway use the axios library to forward requests to the right service.

Developed with ❤️ by Flamingo 
:fireworks: :fireworks: